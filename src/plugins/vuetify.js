import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/dist/vuetify.min.css'
import es from 'vuetify/es5/locale/es'

Vue.use(Vuetify)

export default new Vuetify({
  lang: {
    locales: { es },
    current: 'es'
  },
  theme: {
    options: {
      customProperties: true,
    },
		themes: {
			light: {
        'base-verde': '#03A65A',
        'base': '#FFFFFF',
				'base-verde-oscuro': '#036B40',
				'base-azul-oscuro': '#036B40',
        'base-negro': '#000000',
        'base-amarillo': '#FFA321'
      },
      
      dark: {
        'background': '#000000',
        'base': '#000000',
        'base-azul': '#03A65A',
				'base-verde-oscuro': '#036B40',
				'base-azul-oscuro': '#036B40',
        'base-negro': '#000000',
        'base-amarillo': '#FFA321'
      }
    },
    dark: true
	}
})
