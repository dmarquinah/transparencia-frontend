import Vue from 'vue'
import VueRouter from 'vue-router'
import goTo from 'vuetify/es5/services/goto'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/home/Index.vue'),
    children: [
      {
        path: '',
        name: 'Inicio',
        component: () => import('@/views/inicio/Index.vue'),
      },
      {
        path: '/transparencia',
        name: 'Transparencia',
        component: () => import('@/views/transparencia/Index.vue'),
      },
      {
        path: '/transparencia/:id',
        name: 'Detalle Transparencia',
        component: () => import('@/views/transparencia/DetalleEmpresa.vue'),
      },
      {
        path: '/funcionamiento',
        name: 'Funcionamiento',
        component: () => import('@/views/funcionamiento/Index.vue'),
      },
      {
        path: '/nosotros',
        name: 'Nosotros',
        component: () => import('@/views/transparencia/Index.vue'),
      },
      // {
      //   path: '*',
      //   name: 'FourOhFour',
      //   component: () => import('@/views/404/Index.vue'),
      // },
    ],
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,

  scrollBehavior: (to, from, savedPosition) => {
    let scrollTo = 0

    if (to.hash) {
      scrollTo = to.hash
      return {
        selector: to.hash
      }
    } else if (savedPosition) {
      scrollTo = savedPosition.y
    }

    return goTo(scrollTo)
  }
})

export default router
