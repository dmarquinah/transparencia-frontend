import ApiService from '../../services/api.service';

export const fetchEmpresa = async ({ commit }, idEmpresa) => {
	commit('user/initRequest', null, { root: true });
	const url = ApiService.getResource('/empresa/' + idEmpresa);
	try {
		commit('user/loadingRequest', true, { root: true });
		let response = await ApiService.get(url);
		commit('setListaAnotaciones', response.data.data);
	} catch (err) {
		commit(
			'user/setError',
			{ isError: true, errorMessage: err.response ? err.response.data.message : err.message },
			{ root: true }
		);
		return false;
	} finally {
		commit('user/loadingRequest', false, { root: true });
	}
};