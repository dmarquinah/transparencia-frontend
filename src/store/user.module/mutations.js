export const initRequest = (state) => {
	state.error = '';
	state.isError = false;
	state.authErrorMessage = '';
	state.authError = false;
};

export const loadingRequest = (state, isLoading) => {
	state.loading = isLoading;
};