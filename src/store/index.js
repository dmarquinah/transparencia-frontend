import Vue from 'vue'
import Vuex from 'vuex'
import user from './user.module'
import empresa from './empresa.module'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    empresa
  }
})
